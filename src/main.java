/* Задачи на полиморфизм
 * 1.	Переопределить метод getName в классе Whale(Кит), чтобы программа выдавала: Я не корова, Я – кит.
 * 2.	Написать метод, который определяет, объект какого класса ему передали, и выводит на экран одну из надписей: Кошка, Собака, Птица, Лампа.
 * 3.	Создать два класса Cat(кот) и Dog(собака), класс Dog(собака) происходит от Cat(кот). Определить метод getChild в классах Cat(кот) и Dog(собака), чтобы кот порождал кота, а собака – собаку.
 */

/**
 *
 * @author Виктор
 */

public class main {
    public static void main(String[] args) {
        Whale whale = new Whale();
        whale.getName();

        printObjectType(new Cat());
        printObjectType(new Dog());
        printObjectType(new Bird());
        printObjectType(new Lamp());  
        
        Pet pet1 = new Cat();
        Pet cat = pet1.getChild();
 
        Pet pet2 = new Dog();
        Pet dog = pet2.getChild();
    }
    
    public static void printObjectType(Object o)
    {
        if (o instanceof Cat) System.out.println("Кошка");
        if (o instanceof Dog) System.out.println("Собака");
        if (o instanceof Bird) System.out.println("Птица");
        if (o instanceof Lamp) System.out.println("Лампа");
    }
    
    public static class Cat extends Pet
    {
        /**
         *
         * @return
         */
        @Override
        public Cat getChild() {
            return new Cat();
        }
    }

    public static class Dog extends Cat
    {
        /**
         *
         * @return
         */
        @Override
        public Dog getChild() {
            return new Dog();
        }
    }

    public static class Bird
    {
    }

    public static class Lamp
    {
    }
    
    public static class Pet
    {
        public Pet getChild()
        {
            return new Pet();
        }
    }
}