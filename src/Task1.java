/* 1. Написать два метода: print(int) и print(String).
 * 3. Написать пять методов print с разными параметрами.
 */

/**
 *
 * @author Виктор
 */
public class Task1 {
    public void print(int a) {
        System.out.println(a);
    }
    public void print(String a) {
        System.out.println(a);
    }
    
    public void print(float a) {
        System.out.println(a);
    }

    public void print(double a) {
        System.out.println(a);
    }

    public void print(char a) {
        System.out.println(a);
    }
}
