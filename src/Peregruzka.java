/* Задачи на перегрузку методов
 * 2. Написать два метода: print(int) и print(Integer).Написать такой код в методе main, чтобы вызвались они оба.
 * 4. Написать public static методы: int min(int, int), long min(long, long), double min(double, double). Каждый метод должен возвращать минимальное из двух переданных в него чисел.
 * 5. Написать public static методы: int max(int, int), long max (long, long), double max (double, double). Каждый метод должен возвращать максимальное из двух переданных в него чисел.
 */

/**
 *
 * @author Виктор
 */
public class Peregruzka {
    public static void main(String[] args) {
        print(1);
        System.out.println(min(3,2));
        System.out.println(min(3,2));
        System.out.println(min(3.6,2.5));
        System.out.println(max(3,2));
        System.out.println(max(3,2));
        System.out.println(max(3.6,2.5));
    }
    
    public static void print(int a) {
    }
    
    public static void print(Integer i) {
    }  
    
    public static int min(int a, int b) {
        return (a < b) ? a : b;
    }

    public static long min(long a, long b) {
        return (a < b) ? a : b;
    }

    public static double min(double a, double b) {
        return (a < b) ? a : b;
    }
    
    public static int max(int a, int b)
    {
        return (a > b) ? a : b;
    }

    public static long max(long a, long b)
    {
        return (a > b) ? a : b;
    }

    public static double max(double a, double b)
    {
        return (a > b) ? a : b;
    }
}
